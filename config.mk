#
#
# Miui camera for AOSP ROMs
#
#

PRODUCT_SOONG_NAMESPACES += \
    vendor/xiaomi/miuicam


CAM_PATH := $(LOCAL_PATH)

# Permissions & libs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64)



# Meme cam
PRODUCT_PACKAGES += \
MiuiExtraPhotoOverlay \
MiuiCamera \
MiuiExtraPhoto

# Props needed
PRODUCT_SYSTEM_PROPERTIES += \
persist.vendor.camera.enableAdvanceFeatures=0x3E7 \
persist.vendor.camera.multicam=TRUE \
persist.vendor.camera.multicam.fpsmatch=TRUE \
persist.vendor.camera.multicam.framesync=1 \
persist.vendor.camera.multicam.hwsync=TRUE \
persist.vendor.camera.privapp.list=com.android.camera \
persist.vendor.camera.picturesize.limit.enable=false \
persist.sys.miui.sdk.dbg \
ro.miui.notch=1 \
ro.miui.ui.version.code=13 \
ro.miui.ui.version.name=V130 \
ro.miui.region=CN \
ro.miui.build.region=cn \
ro.fota.oem=Xiaomi \
ro.boot.camera.config=_pro \
ro.com.google.lens.oem_camera_package=com.android.camera \
ro.control_privapp_permissions=log \

include $(CAM_PATH)/BoardConfigMIUI.mk
